
const electron = require('electron')
const BrowserWindow = electron.BrowserWindow
const Menu = electron.Menu
const app = electron.app

const path = require('path')
const url = require('url')

const ipc = require('electron').ipcMain
const ipcmain =require('electron').ipcMain


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let splashWin;

// ****************************************************************************
// Menu Related
// ****************************************************************************
let template = [
    {
        label: 'Trading',
        ent: 13,
        submenu: [
            {
                label: 'Order List',
                ent: 50,
                submenu: [
                    { label: 'All Orders',
                        ent: 104,
                        click: function () {
                            setMenus('all orders pressed',104);
                        }
                    },
                    { label: 'Open Orders', ent: 103 },
                    { label: 'Waiting for Approval', ent: 140 },
                    { label: 'Executed Orders', ent: 105 },
                ],
            },
            { label: 'Trading Connections', ent: 1069 },
        ],
    },
    {
        label: 'Customers',
        ent: 17,
        submenu: [
            { label: 'Customers', ent: 12,
                click: function () {
                    setMenus('customer pressed',12);
                }},
            { label: 'Cash Accounts', ent: 20 },
            { label: 'Security Accounts', ent: 21 },
            { label: 'Trading Accounts', ent: 35 },
            { label: 'Ac`count Locked Customers', ent: 623 },
        ],
    },
    {
        label: 'Finance',
        ent: 84,
        submenu: [
            {
                label: 'Customer Notifications',
                ent: 121,
                submenu: [
                    { label: 'Deposit', ent: 526 },
                    { label: 'Withdrawal', ent: 527 },
                    { label: 'Cash Transfer', ent: 1951 },
                ],
            },
            {
                label: 'Cash Management',
                ent: 98,
                submenu: [
                    { label: 'Pending Transactions', ent: 114 },
                    { label: 'All Transactions', ent: 528 },
                    { label: 'Deposits', ent: 116 },
                    { label: 'Withdrawals', ent: 117 },
                    { label: 'Cash Transfers', ent: 1505 },
                ],
            },
            { label: 'SWIFT Transactions', ent: 1009 },
            { label: 'Market Cap Notifications', ent: 1788 },
        ],
    },
    {
        label: 'Master Data',
        ent: 132,
        submenu: [
            {
                label: 'Symbol Management',
                ent: 510,
                submenu: [
                    { label: 'Symbols', ent: 223 },
                    { label: 'Sectors', ent: 224 },
                ],
            },
            {
                label: 'Finance',
                ent: 511,
                submenu: [
                    { label: 'Banks', ent: 229 },
                    { label: 'Cost Centers', ent: 323 },
                    { label: 'Currencies', ent: 226 },
                    { label: 'Signup Locations', ent: 228 },
                    { label: 'Countries', ent: 227 },
                ],
            },
        ],
    },
    {
        label: 'System',
        ent: 589,
        submenu: [
            { label: 'Institutions', ent: 1277 },
            {
                label: 'Exchange',
                ent: 509,
                submenu: [
                    { label: 'Exchanges', ent: 222 },
                    { label: 'Trading Markets', ent: 171 },
                ],
            },
        ],
    },

    {
        label: '                    ',
        role: '',
    },



    {
        label: 'Window',
        role: 'window',
        submenu: [{
            label: 'Minimize',
            accelerator: 'CmdOrCtrl+M',
            role: 'minimize'
        }, {
            label: 'Close',
            accelerator: 'CmdOrCtrl+W',
            role: 'close'
        }, {
            type: 'separator'
        }, {
            label: 'Reopen Window',
            accelerator: 'CmdOrCtrl+Shift+T',
            enabled: false,
            key: 'reopenMenuItem',
            click: function () {
                app.emit('activate')
            }
        }]
    }, {
        label: 'Help',
        role: 'help',
        submenu: [{
            label: 'Learn More',
            click: function () {
                electron.shell.openExternal('http://electron.atom.io')
            }
        }]
    }
];

function addUpdateMenuItems(items, position) {
    if (process.mas) return

    const version = electron.app.getVersion()
    let updateItems = [{
        label: `Wrapper version ${version}`,
        enabled: false
		/*}, {
		 label: 'Checking for Update',
		 enabled: false,
		 key: 'checkingForUpdate'
		 }, {
		 label: 'Check for Update',
		 visible: false,
		 key: 'checkForUpdate',
		 click: function () {
		 require('electron').autoUpdater.checkForUpdates()
		 }
		 }, {
		 label: 'Restart and Install Update',
		 enabled: true,
		 visible: false,
		 key: 'restartToUpdate',
		 click: function () {
		 require('electron').autoUpdater.quitAndInstall()
		 }*/
    }]

    items.splice.apply(items, [position, 0].concat(updateItems))
}

function findReopenMenuItem() {
    const menu = Menu.getApplicationMenu()
    if (!menu) return

    let reopenMenuItem
    menu.items.forEach(function (item) {
        if (item.submenu) {
            item.submenu.items.forEach(function (item) {
                if (item.key === 'reopenMenuItem') {
                    reopenMenuItem = item
                }
            })
        }
    })
    return reopenMenuItem
}

if (process.platform === 'darwin') {
    const name = electron.app.getName()
    template.unshift({
        label: name,
        submenu: [{
            label: `About ${name}`,
            role: 'about'
        }, {
            type: 'separator'
        }, {
            label: 'Reload',
            accelerator: 'CmdOrCtrl+R',
            click: function (item, focusedWindow) {
                if (focusedWindow) {
                    // on reload, start fresh and close any old
                    // open secondary windows
                    if (focusedWindow.id === 1) {
                        BrowserWindow.getAllWindows().forEach(function (win) {
                            if (win.id > 1) {
                                win.close()
                            }
                        })
                    }
                    focusedWindow.reload()
                }
            }
        }, {
            label: 'Toggle Full Screen',
            accelerator: (function () {
                if (process.platform === 'darwin') {
                    return 'Ctrl+Command+F'
                } else {
                    return 'F11'
                }
            })(),
            click: function (item, focusedWindow) {
                if (focusedWindow) {
                    focusedWindow.setFullScreen(!focusedWindow.isFullScreen())
                }
            }
        }, {
            label: 'Toggle Developer Tools',
            accelerator: (function () {
                if (process.platform === 'darwin') {
                    return 'Alt+Command+I'
                } else {
                    return 'Ctrl+Shift+I'
                }
            })(),
            click: function (item, focusedWindow) {
                if (focusedWindow) {
                    focusedWindow.toggleDevTools()
                }
            }
        }, {
            type: 'separator'
        }, {
            label: `Hide ${name}`,
            accelerator: 'Command+H',
            role: 'hide'
        }, {
            label: 'Hide Others',
            accelerator: 'Command+Alt+H',
            role: 'hideothers'
        }, {
            label: 'Show All',
            role: 'unhide'
        }, {
            type: 'separator'
        }, {
            label: 'Quit',
            accelerator: 'Command+Q',
            click: function () {
                app.quit()
            }
        }]
    })

    // Window menu.
    template[6].submenu.push({
        type: 'separator'
    }, {
        label: 'Bring All to Front',
        role: 'front'
    })

    addUpdateMenuItems(template[0].submenu, 1)
}

if (process.platform === 'win32') {
    const helpMenu = template[template.length - 1].submenu
    addUpdateMenuItems(helpMenu, 0)
}

// TODO: [Amila] This method is added short term basis
function setMenus(str , entitlement){
    // mainWindow.webContents.send('open-file');
    console.log(str);
    mainWindow.webContents.send('send-click-to-AT',entitlement);

    console.log('rachel');
    console.log('ross');
}


function sendMessage(focusedWindow){
    mainWindow.webContents.executeJavaScript("myFunction2()");
}

//sends pong to listening
var varValue;
//receives from rubix
ipc.on('send-menu-to-electron', function (event, arg) {
    console.log('start');


    // arg.MENU_METADATA.forEach(function (val) {
    // 	var newMenu = {
    // 		label: val['label'],
    // 		submenu: [],
    // 	};
    // 	if (val['submenu']) {
    // 		val['submenu'].forEach(function (ri) {
    // 			var subItem = {
    // 				label: ri['label'],
    // 				submenu: [],
    // 			};
    // 			if (ri['submenu']) {
    // 				ri['submenu'].forEach(function (subRI) {
    // 					subItem.submenu.push({
    // 						label: subRI['label'],
    // 					});
    // 				});
    // 			}
    // 			newMenu.submenu.push(subItem);
    // 		});
    // 	}
    // 	template.push(newMenu);
    // 	console.log('end');
    // });


// 	arg.MENU_METADATA.forEach(val => {
//
// 		const newMenu = {
// 			label: val['label'],
// 			submenu: [],
// 		},
// 		if (val['submenu']) {
// 			val['submenu'].forEach(ri => {
// 				const subItem = {
// 					label: ri['label'],
// 					submenu: [],
// 				},
// 			if (ri['submenu']) {
// 				ri['submenu'].forEach(subRI => {
// 					subItem.submenu.push({
// 					label: subRI['label'],
// 				});
// 			});
// 			}
// 			newMenu.submenu.push(subItem);
// 		});
// 		}
// 		template.push(newMenu);
// 	}
// },
    console.log(arg.MENU_METADATA);
    // const menu = Menu.buildFromTemplate(template)
    // Menu.setApplicationMenu(menu)
    var template2 = makeTemplate(arg);
    makeMenu(arg);
    returnMenu(arg);

    // $(arg.MENU_METADATA).click(function () {
    // 	// ipc.send('openChildWindow')
    // 	event.sender.send('asynchronous-reply', arg)
    // })

})



function returnMenu(arg){
    return arg;
}

function makeTemplate(arg){
    // const newMenu = {
    // 		label: val['disp'],
    // 		submenu: [],
    // 	};
    var sample = [];
    for (var key in arg.MENU_METADATA) {
        var obj = arg.MENU_METADATA[key];
        sample.push(obj);
        console.log(obj.label);
    }
    for( i=0; i<5 ;i++)
    {
        for (var varName in arg.MENU_METADATA[i]) {
            varValue=arg.MENU_METADATA[i][varName]
            console.log( varName + '-' + varValue);
        }
    }
    // setArray(sample)
    return sample;
}

function makeMenu(arg){
	console.log('class');
}


function showToBeImplementedMessage(focusedWindow) {
    const options = {
        type: 'info',
        title: 'Information',
        buttons: ['Ok'],
        message: 'This feature is not developed yet. Please wait for future releases. Thanks for your intrest shown in our app...!'
    }
    electron.dialog.showMessageBox(focusedWindow, options, function () {})
}

function postMenuClickToRubix(item, focusedWindow) {
    postMessageToRubix("{channel: 'Wrapper', message: 'Message sent from electron'}");
}

function postMenuClickToAT(item,focusedWindow){
    postMessageToAT("{channel: 'Wrapper', message: 'Message sent from electron'}")
}
// ****************************************************************************
// End Menu Related
// ****************************************************************************

function postMessageToRubix(msg) {
    mainWindow.webContents.executeJavaScript("window.postMessage(["+msg+"], '*')");
}

function postMessageToAT(msg) {
    mainWindow.webContents.executeJavaScript("");
}

function createWindow() {
    // Create the browser window.
    app.commandLine.appendSwitch('disable-web-security');
    mainWindow = new BrowserWindow({

        'web-preferences': {'web-security': false},
        width: 1800,
        height: 1600,
        show: false,
        minWidth: 1024,
        minHeight: 768

    });

    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)

    // mainWindow.loadURL(`https://rubix.mubashertrade.com/rubix-global/desktop`);
    mainWindow.loadURL(`http://127.0.0.1:4200/login`);

    // Open the DevTools.


    //browserWindow.setMenu(null);
    mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        mainWindow = null
    })

    mainWindow.webContents.on('did-finish-load', () => {
        if (splashWin) {
            splashWin.close();
        }

        // mainWindow.maximise();
        mainWindow.show();

    // Pass the version information to Rubix
    let platform = 'windows';
    if (process.platform !== 'darwin') {
        platform = 'macos'
    }

    msg = "{'os':'" + platform + "','wrapper_type':'desktop_wrapper','wrapperVersion':'" + "1.0.0" + "'}";
    postMessageToRubix(msg);
    postMessageToAT(msg);
})
}

app.on('ready', function () {
    createWindow();

    const modalPath = path.join('file://', __dirname, 'assets/splash/splash.html')
    splashWin = new BrowserWindow({
        width: 576,
        height: 332,
        show: true,
        frame: false,
        alwaysOnTop: true
    });

    splashWin.on('close', function () { splashWin = null })
    splashWin.loadURL(modalPath)
    splashWin.show()


})


app.on('window-all-closed', function () {

    if (process.platform !== 'darwin') {
        app.quit()
    }

    let reopenMenuItem = findReopenMenuItem()
    if (reopenMenuItem) reopenMenuItem.enabled = true
})

app.on('browser-window-created', function () {
    let reopenMenuItem = findReopenMenuItem()
    if (reopenMenuItem) reopenMenuItem.enabled = false
})

app.on('activate', function () {

    if (mainWindow === null) {
        createWindow()
    }
})
